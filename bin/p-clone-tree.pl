#!/usr/bin/perl
use strict;
use warnings;
use 5.010;
use File::Spec::Functions ':ALL';
use File::Find;
use File::Copy;
use File::Temp 'tempdir';
use Archive::Zip;

my $path_in  = shift;
my $path_out = shift;

$path_in  = '/cygdrive/c/Temp/in/' unless $path_in;
$path_out = '/cygdrive/c/Temp/out/' unless $path_out;

$path_in = rel2abs ($path_in);
$path_out = rel2abs ($path_out);

system ('mkdir', '-p', $path_in);
system ('mkdir', '-p', $path_out);

find (\&_copy_files, $path_in);

exit 0;

sub _copy_files
{
    my $path_in_name = $File::Find::name;
    my $path_in_dir = $File::Find::dir;
    my $rel_name = abs2rel ($path_in_name, $path_in);
    my $rel_dir  = abs2rel ($path_in_dir,  $path_in);
    my $path_out_name = rel2abs ($rel_name, $path_out);
    my $path_out_dir = rel2abs ($rel_dir, $path_out);

    # sometimes Windows 8.3 shortened names appear in addition to full paths
    if ($path_in_name =~ /[^\/]~[0-9]/)
    {
        say "Skipping: $path_in_name";
        return;
    }

    system ('mkdir', '-p', $path_out_dir) unless -d $path_out_dir;
    if (-d $path_in_name)
    {
        say $path_in_name;
        return;
    }
    return unless -e $path_in_name;
    return if -e $path_out_name;

    my $type = $path_in_name;
    $type =~ s/^.*\.//;
    return unless $type;
    $type = lc $type;
    if ($type =~ /^(docx?|xlsx?|msg|txt)$/)
    {
        # just copy Office docs
        say "Copying: $path_in_name";
        copy ($path_in_name, $path_out_name);
    }
    elsif ($type =~ /^(jpg|jpeg|db|tif|tiff|psd)$/)
    {
        say "skipping photo";
        return;
    }
    elsif ($type eq 'pdf')
    {
        # create a output folder called /out/finename.pdf/
        # fill it with single page pdfs
        say "Splitting: $path_in_name";
        if ($path_out_name =~ /%/)
        {
            # can't cope with files with % characters, wait for finish before renaming folder
            my $tmp_path_out_name = $path_out_name;
            $tmp_path_out_name =~ s/%/__PERCENT__/gx;
            system('mkdir', '-p', $tmp_path_out_name);
            system("pdfseparate \"$path_in_name\" \"$tmp_path_out_name/\%d.pdf\"");
            rmdir($path_out_name);
            rename($tmp_path_out_name, $path_out_name);
        }
        else
        {
            system('mkdir', '-p', $path_out_name);
            system("pdfseparate \"$path_in_name\" \"$path_out_name/\%d.pdf\" &");
        }
    }
    elsif ($type eq 'zip')
    {
        # unzip into a temp folder, then fork p-clone-tree.pl to copy files and separate/unzip further
        $path_out_name =~ s/\.zip$//i;
        return if -d $path_out_name;
        say "unzipping: $path_in_name";
        my $tempdir = tempdir( CLEANUP => 1 );
        #system ('unzip', $path_in_name, '-d', $tempdir);
        #system ('7z', 'x', $path_in_name, '-o' . $tempdir);
        my $zip = Archive::Zip->new;
        $zip->read ($path_in_name);
        $zip->extractTree ('', $tempdir);
        system ($0, $tempdir, $path_out_name);
    }
    elsif ($type eq '7z')
    {
        # unzip into a temp folder, then fork p-clone-tree.pl to copy files and separate/unzip further
        $path_out_name =~ s/\.7z$//i;
        return if -d $path_out_name;
        say "un7zipping: $path_in_name";
        my $tempdir = tempdir( CLEANUP => 1 );
        system ('7z', 'x', $path_in_name, '-bso0', '-bb0', '-bd', '-o' . $tempdir);
        system ($0, $tempdir, $path_out_name);
    }
    else
    {
        say "WARNING: Unknown type $type";
        say "Copying: $path_in_name";
        copy ($path_in_name, $path_out_name);
    }
}
