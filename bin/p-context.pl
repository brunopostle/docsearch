#!/usr/bin/perl
use strict;
use warnings;
use Digest::MD5;
use 5.010;

my $space = '(?:\302\240|[[:space:]-])*';
my $path_base = 'I:\CURRENT Jobs\DOS Path to\Incoming Documents';
my $regex = "(?:24|twenty $space four) $space (?:tonne|ton|t [[:space:]])";
#my $regex = "free $space issued? .{0,180} concrete|concrete .{0,180} free $space issued?";
#my $regex = "drawings? .{0,30} (?:laydown|pavement|road|yard)s?|(?:laydown|pavement|road|yard)s? .{0,30} drawings?";

say '<html><head><meta charset="UTF-8" /></head><body>';

my $lookup = {};
my $output = [];

for my $path_rel (<STDIN>)
{
    chomp $path_rel;
    $path_rel =~ s/^\.\///x;
    next unless $path_rel =~ /\.txt$/;

    # read the file and try to extract the matching text
    local $/ = undef;
    open my $fh, '<:crlf', $path_rel;
    my $text_full = <$fh>;
    close $fh;

    # this is a bit like grep -C2
    my @result = $text_full =~ m/(
                                   (?:
                                     (?:\n[^\n]*?){0,2}
                                     (?:$regex)
                                     (?:[^\n]*?\n){0,3}
                                   )+
                                 )/gsxi;

    next unless scalar @result;

    my $md5 = new Digest::MD5;
    $md5->add($text_full);
    my $hash = $md5->hexdigest();
    my $tmp_output = [[],];

    my $path_rel_dos = $path_rel;
    $path_rel_dos =~ s/\//\\/g;
    $path_rel_dos =~ s/(?:\.index)?\\([0-9]+)\.txt ?$/.pdf/;
    $path_rel_dos =~ s/\\([0-9]+)\.txt ?$//;
    my $page = $1;
    my $path_full_dos = "$path_base\\$path_rel_dos";
    my $length_dos = length $path_full_dos;
    $path_full_dos =~ s/&/&amp;/g;

    if (defined $page and $page > 0)
    {
        # we have a pdf page split like so: /path/to/doc.pdf/001.txt
        push @{$tmp_output->[0]}, "<p><font face=\"Calibri\"><a href=\"$path_full_dos\">$path_full_dos</a> Page $page</font></p>";
    }
    else
    {
        # for non-pdf and unsplit pdf, figure out what the file extension of the original file was
        my $path_rel_prefix = $path_rel;
           $path_rel_prefix =~ s/\.txt$//;
        my @files = glob "'$path_rel_prefix.*'";
        @files = grep (!/\.txt$/, @files);
        @files = 'some bogus filename.txt' unless scalar @files;
        my $extension = shift @files;
        $extension =~ s/.*\.//;

        my $path_full_dos_prefix = $path_full_dos;
           $path_full_dos_prefix =~ s/\.txt$//;

        push @{$tmp_output->[0]}, "<p><font face=\"Calibri\"><a href=\"$path_full_dos_prefix.$extension\">$path_full_dos_prefix.$extension</a></font></p>";
    }
    push @{$tmp_output->[0]}, "<p><font color=\"red\">Warning: Path length $length_dos longer than 260 chars<\/font></p>" if $length_dos > 260;

    if (defined $lookup->{$hash})
    {
        push @{$lookup->{$hash}->[0]}, @{$tmp_output->[0]};
        next;
    }

    map {$_ =~ s/&/&amp;/g} @result;
    map {$_ =~ s/</&lt;/g} @result;
    map {$_ =~ s/>/&gt;/g} @result;
    map {$_ =~ s/($regex)/<font color="red">$1<\/font>/gsxi} @result;

    my $result = join "<font color=\"red\">--<\/font>\n", @result;

    $result =~ s/\n{2,5}/\n\n/g;
    $result =~ s/\n+//;
    $tmp_output->[1] = "<pre><font face=\"Consolas\">$result</font></pre>";

    $lookup->{$hash} = $tmp_output;

    push @{$output}, $tmp_output;
}

for my $tmp_output (@{$output})
{
    say $_ for (@{$tmp_output->[0]});
    say $tmp_output->[1];
}

say '</body></html>';

0;

