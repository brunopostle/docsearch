#!/bin/bash

# one-liner to extract text from docx files
# https://stackoverflow.com/questions/5671988/how-to-extract-just-plain-text-from-doc-docx-files-unix

unzip -p "$1" word/document.xml | sed -e 's/<\/w:p>/\n/g; s/<[^>]\{1,\}>//g; s/[^[:print:]\n]\{1,\}//g' > "$2"

