#!/usr/bin/python3

import sys
import re
import csv
import fitz

"""
2024 Bruno Postle
SPDX:GPL-3.0-or-later

Extracts fields from PDF drawing boxes. Assumes all black outlined boxes are
table cells and contain textual key/value data.

PDFs need to be rotated correctly, and may need rotating even if they 'appear'
to be the right way up

If 100% vector PDFs then maybe use OCRmyPDF first. Scanned PDFs you are out of
luck.

Edit 'fieldnames' to suit your dataset.

"""

fieldnames = [
    "Path",
    "Client",
    "Job No",
    "Job Number",
    "Job Title",
    "Project",
    "Drawing No",
    "Drawing Number",
    "Revision",
    "Drawing Title",
    "Title",
    "Date",
    "Scale",
    "Drawn",
    "Checked by",
]


def flatten(S):
    if S == []:
        return S
    if isinstance(S[0], list):
        return flatten(S[0]) + flatten(S[1:])
    return S[:1] + flatten(S[1:])


rows = []
for path in sys.stdin:
    path = path.rstrip()
    doc = fitz.open(path)
    page_results = {}
    for key in fieldnames:
        page_results[key] = ""
    page_results["Path"] = path
    for table in doc[0].find_tables():
        for cell in flatten(table.extract()):
            if isinstance(cell, str) and cell != "":
                cell = " ".join(cell.splitlines())
                for key in fieldnames:
                    if re.match("[.]? *" + key, cell, re.IGNORECASE):
                        value = re.findall(
                            r"[.]? *" + key + "[:.]?[ ]+(.*)",
                            cell,
                            re.IGNORECASE,
                        )
                        if value:
                            page_results[key] = value[0]
    rows.append(page_results)

with open("output.csv", "w", encoding="UTF8", newline="") as f:
    writer = csv.DictWriter(f, fieldnames=fieldnames)
    writer.writeheader()
    writer.writerows(rows)
