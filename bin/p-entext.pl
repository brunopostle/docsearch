#!/usr/bin/perl
use strict;
use warnings;
use 5.010;

my $total = scalar @ARGV;
my $inc = 0;

for my $path (@ARGV)
{
    $inc++;
    next unless -e $path;

    my $type = $path;
    $type =~ s/^.*\.//;
    next unless $type;
    $type = lc $type;

    my $path_base = $path;
    $path_base =~ s/\.[[:alnum:]]+$//;

    my $path_txt = "$path_base.txt";
    next if -e $path_txt;

    # create TXT versions of PDF file, OCR the first page if necessary
    # (First use `p-pdfsplit` to split the PDF file into pages)
    if ($type eq 'pdf' and $path =~ /\.(index|pdf|PDF)\/[0-9]+\.pdf$/)
    {
        system ('pdftotext', $path, $path_txt);
        if (-s $path_txt < 5)
        {
            unless (-e "$path_base.png")
            {
                system ('pdftoppm', '-rx', 300, '-ry', 300, '-singlefile', $path, $path_base);
                system ('convert', "$path_base.ppm", "$path_base.png");
                unlink "$path_base.ppm";
            }
            system ('tesseract', "$path_base.png", $path_base, '--psm', 1);
        }
    }

    elsif ($type eq 'doc')
    {
        next if $path =~ /~\$/;
        $path = quotemeta($path);
        $path_txt = quotemeta($path_txt);
        `antiword $path > $path_txt`;
    }

    elsif ($type eq 'docx')
    {
        system ('p-docx2text.sh', $path, $path_txt);
    }

    elsif ($type eq 'msg')
    {
        system('msgconvert', '--outfile', $path_txt, $path);
    }

    elsif ($type =~ /^xls$/)
    {
        #system ('xls2csv', '-o', "$path_base.csv", $path);
        `xls2csv "$path" > "$path_base.csv"`;
        rename "$path_base.csv", $path_txt;
    }

    elsif ($type =~ /^html?$/)
    {
        `html2text --ignore-images --ignore-emphasis --decode-errors=ignore "$path" > "$path_txt"`;
    }

    else {next}

    system ('unix2dos', '-q', $path_txt);
    say STDERR $path_txt;
}

0;

