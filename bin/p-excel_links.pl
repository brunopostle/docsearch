#!/usr/bin/perl
use strict;
use warnings;
use 5.010;
use Cwd;
use Excel::Writer::XLSX;

# generates a clickable-link excel spreadsheet from a list of files

my $path_xlsx = 'file_list.xlsx';
unlink $path_xlsx if (-e $path_xlsx);
my $workbook = Excel::Writer::XLSX->new($path_xlsx) || die "can't create $path_xlsx";
my $worksheet = $workbook->add_worksheet();

my $cwd = Cwd::cwd();
#$cwd =~ s/^\/cygdrive\/([a-z0-9]+)/external:\\\\?\\$1:/;
$cwd =~ s/^\/cygdrive\/([a-z0-9]+)/external:$1:/;

$worksheet->write(0, 0, "Folder");
$worksheet->write(0, 1, "Filename");
$worksheet->write(0, 2, "Type");
$worksheet->write(0, 3, "Size (kB)");
$worksheet->write(0, 4, "Path");

my $row_id = 1;
for (<>)
{
    chomp;
    warn $_ unless -e $_;
    next unless -f $_;
    next if $_ =~ /file_list\.xlsx$/;
    next if $_ =~ /~\$/;
    next if $_ =~ /Thumbs\.db$/;
    next if $_ =~ /desktop\.ini$/i;
    my $rel_path = $_;
    $rel_path =~ s/^\.\///;

    my $filename = $_;
    $filename =~ s/.*\///;

    my $extension = uc($filename);
    $extension =~ s/.*\.//;

    my $abs_path = $cwd .'/'. $rel_path;

    my $size = -s $_;

    my $folder_path = $_;
    if ($folder_path =~ /\//)
    {
        $folder_path =~ s/\/[^\/]*$/\//;
    }
    else
    {
        $folder_path = '';
    }

    my $folder_path_win = $folder_path;
    $folder_path_win =~ s/^\.\///;
    $folder_path_win =~ s/\//\\/g;
    $folder_path_win = '.\\' if $folder_path_win eq '';

    my $abs_folder_path = $cwd .'/'. $folder_path;

    my $rel_path_win = $rel_path;
    $rel_path_win =~ s/\//\\/g;

    $worksheet->write_url($row_id, 0, $abs_folder_path, undef, $folder_path_win);
    $worksheet->write_url($row_id, 1, $abs_path, undef, $filename);
    $worksheet->write($row_id, 2, $extension);
    $worksheet->write($row_id, 3, int(($size/1024)+0.5));
    $worksheet->write($row_id, 4, $rel_path_win);

#Archive:  Some File Name With Q10 (Additional Transfer 9).zip
#  Length      Date    Time    Name
#---------  ---------- -----   ----
#   126030  02-04-2020 11:01   Casselle 1/1 Late 1.JPG
#   185473  02-04-2020 11:01   Casselle 1/1 Late 10.JPG
#   173556  02-04-2020 11:01   Casselle 1/1 Late 2.JPG
#   170163  02-04-2020 11:01   Casselle 1/1 Late 3.JPG

    if ($filename =~ /\.zip$/i)
    {
        my $stub = $filename;
        $stub =~ s/\.zip$//i;
        next if -d $stub; # zip has presumably already been extracted
        my $rel_path_quoted = quotemeta($rel_path);
        my $table = `unzip -l $rel_path_quoted`;
        my @items = $table =~ m/ *([0-9]+ *..-..-.... ..:.. .*)/g;
        for my $item (@items)
        {
            my ($size, $time, $file) = $item =~ m/([0-9]+) *(..-..-.... ..:..)(.*)/;
            $file =~ s/^ *//; $file =~ s/ *$//;
            next if $file =~ /\/$/; # skip directory entries within ZIP
            next if $file =~ /Thumbs\.db$/;
            next if $file =~ /desktop\.ini$/i;
            if ($file =~ /\.zip$/i)
            {
                say "Warning: ZIP file within ZIP file! not recursing:";
                say $file;
            }
            $file =~ s/\//\\/g;
            my $extension = uc($file);
            $extension =~ s/.*\.//;

            $row_id++;
            $worksheet->write_url($row_id, 0, $abs_path, undef, $rel_path_win);
            $worksheet->write($row_id, 1, $file);
            $worksheet->write($row_id, 2, $extension);
            $worksheet->write($row_id, 3, int(($size/1024)+0.5));
            $worksheet->write($row_id, 4, $rel_path_win.'\\'.$file);
        }
    }

    $row_id++;
}

$workbook->close();

0;
