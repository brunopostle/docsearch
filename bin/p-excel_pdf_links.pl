#!/usr/bin/perl
use strict;
use warnings;
use 5.010;
use Cwd;
use Encode 'decode';
use Date::Manip qw(ParseDate UnixDate);
use Excel::Writer::XLSX;

# generates a clickable-link excel spreadsheet from a list of PDF files

my $path_xlsx = 'file_list_pdf.xlsx';
unlink $path_xlsx if (-e $path_xlsx);
my $workbook = Excel::Writer::XLSX->new($path_xlsx) || die "can't create $path_xlsx";
my $worksheet = $workbook->add_worksheet();

my $cwd = Cwd::cwd();
#$cwd =~ s/^\/cygdrive\/([a-z0-9]+)/external:\\\\?\\$1:/;
$cwd =~ s/^\/cygdrive\/([a-z0-9]+)/external:$1:/;

$worksheet->write(0, 0, "Folder");
$worksheet->write(0, 1, "Filename");
$worksheet->write(0, 2, "Size");
$worksheet->write(0, 3, "Orientation");
$worksheet->write(0, 4, "Pages");
$worksheet->write(0, 5, "Producer");
$worksheet->write(0, 6, "Creator");
$worksheet->write(0, 7, "Title");
$worksheet->write(0, 8, "Author");
$worksheet->write(0, 9, "CreationDate");
$worksheet->write(0,10, "ModDate");

my $row_id = 1;
for (<>)
{
    chomp;
    warn $_ unless -e $_;
    my $rel_path = $_;
    $rel_path =~ s/^\.\///;

    my $filename = $_;
    $filename =~ s/.*\///;

    my $abs_path = $cwd .'/'. $rel_path;

    my $folder_path = $_;
    $folder_path =~ s/\/[^\/]*$//;
    
    my $abs_folder_path = $cwd .'/'. $folder_path;

    $worksheet->write_url($row_id, 0, $abs_folder_path, undef, $folder_path);
    $worksheet->write_url($row_id, 1, $abs_path, undef, $filename);

    my $path_shell = quotemeta $rel_path;
    my $info = `pdfinfo $path_shell`;

    if ($info =~ m/Page size: *([0-9.]+) x ([0-9.]+) pts/)
    {
        my $width_cm = pts2cm($1);
        my $height_cm = pts2cm($2);
        my $size = paper_size($width_cm * $height_cm);
        my $orientation = orientation ($width_cm, $height_cm);

        $worksheet->write($row_id, 2, "$size");
        $worksheet->write($row_id, 3, "$orientation");
    }
    if ($info =~ m/Pages: *(.*)/)
    {
        $worksheet->write($row_id, 4, "$1");
    }
    if ($info =~ m/Producer: *(.*)/)
    {
        $worksheet->write($row_id, 5, decode('UTF8', $1));
    }
    if ($info =~ m/Creator: *(.*)/)
    {
        $worksheet->write($row_id, 6, decode('UTF8', $1));
    }
    if ($info =~ m/Title: *(.*)/)
    {
        $worksheet->write($row_id, 7, decode('UTF8', $1));
    }
    if ($info =~ m/Author: *(.*)/)
    {
        $worksheet->write($row_id, 8, decode('UTF8', $1));
    }
    if ($info =~ m/CreationDate: *(.*)/)
    {
        $worksheet->write($row_id, 9, _date($1));
    }
    if ($info =~ m/ModDate: *(.*)/)
    {
        $worksheet->write($row_id, 10, _date($1));
    }

    $row_id++;
}

$workbook->close();

sub _date
{
    my ($date_pdf) = @_;
    my $date = ParseDate($date_pdf);
    UnixDate($date,"%Y-%m-%d");
}

sub orientation
{
    my ($width, $height) = @_;
    return 'Portrait' if $width < $height;
    return 'Landscape' if $width > $height;
    return 'Square';
}

sub paper_size
{
    my $area_cm = shift;
    # a sheet of A0 paper is 1m^2, (10000cm^2)
    return 'A0' if ($area_cm > 7500);    # 7500.0
    return 'A1' if ($area_cm > 7500/2);  # 3750.0
    return 'A2' if ($area_cm > 7500/4);  # 1875.0
    return 'A3' if ($area_cm > 7500/8);  #  937.5
    return 'A4' if ($area_cm > 7500/16); #  468.75
    return 'A5' if ($area_cm > 7500/32); #  234.375
    return 'A6';
}

sub pts2cm
{
    my $pts = shift;
    return $pts * 127 / 3600;
}

0;
