#!/usr/bin/perl
use 5.010;
use strict;
use warnings;
use DBI;

my $path_db = 'words.db';
say "Using database: $path_db";

my $dbh = DBI->connect(          
    "dbi:SQLite:dbname=$path_db", 
    "",                          
    "",                          
    { RaiseError => 1 },         
) or die $DBI::errstr;

my $words = $dbh->selectcol_arrayref ('SELECT Word, Path from Words', { Columns=>[1,2] });
my $db;

while (scalar @$words)
{
    my $word = shift @$words;
    my $path = shift @$words;
    $db->{lc $word}->{$path} = 1;
}

open HTML, '>', 'index.html';

say HTML '<html><head><title>PDF Index</title></head>';
say HTML '<body><h1>PDF Index</h1>';

for my $word (sort keys %{$db})
{
    say HTML "<h4>$word</h2>";
    say HTML '<p>';
    my $path_id = 1;
    for my $path (sort keys %{$db->{$word}})
    {
        $path =~ s/\.txt$//;
        say HTML "<a href=\"$path\">$path_id</a> ";
        $path_id++;
    }
    say HTML '</p>';
}

say HTML '</body></html>';
close HTML;

0;

