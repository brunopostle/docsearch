#!/usr/bin/perl
use strict;
use warnings;
use DBI;
use 5.010;

my $path_db = 'words.db';
say "Using database: $path_db";

my $dbh = DBI->connect(          
    "dbi:SQLite:dbname=$path_db", 
    "",                          
    "",                          
    { RaiseError => 1, AutoCommit => 0 },         
) or die $DBI::errstr;

$dbh->do("CREATE TABLE Papers(Id INTEGER PRIMARY KEY, Path TEXT, Size TEXT, Orientation TEXT)") unless (grep /Papers/, $dbh->tables);

say "Files: ". scalar @ARGV;
for my $path_pdf (@ARGV)
{
    next unless $path_pdf =~ /\.(index|pdf)\/[0-9]+.pdf$/;
    my $path_shell = quotemeta $path_pdf;

    my $info = `pdfinfo $path_shell`;
    $info =~ m/Page size: *([0-9.]+) x ([0-9.]+) pts/;

    my $width_cm = pts2cm($1);
    my $height_cm = pts2cm($2);
    my $size = paper_size($width_cm * $height_cm);
    my $orientation = orientation ($width_cm, $height_cm);

    my $path_quoted = $dbh->quote ($path_pdf);

    my @rv = $dbh->selectrow_array("SELECT Size from Papers WHERE Path = $path_quoted");
    if (scalar @rv)
    {
        say "Skipping $path_pdf";
    }
    else
    {
        say "Updating $path_pdf";
        $dbh->do("INSERT INTO Papers(Path, Size, Orientation) VALUES($path_quoted, '$size', '$orientation')");
    }
}

$dbh->commit();
$dbh->disconnect();

sub orientation
{
    my ($width, $height) = @_;
    return 'Portrait' if $width < $height;
    return 'Landscape' if $width > $height;
    return 'Square';
}

sub paper_size
{
    my $area_cm = shift;
    # a sheet of A0 paper is 1m^2, (10000cm^2)
    return 'A0' if ($area_cm > 7500);    # 7500.0
    return 'A1' if ($area_cm > 7500/2);  # 3750.0
    return 'A2' if ($area_cm > 7500/4);  # 1875.0
    return 'A3' if ($area_cm > 7500/8);  #  937.5
    return 'A4' if ($area_cm > 7500/16); #  468.75
    return 'A5' if ($area_cm > 7500/32); #  234.375
    return 'A6';
}

sub pts2cm
{
    my $pts = shift;
    return $pts * 127 / 3600;
}

0;
