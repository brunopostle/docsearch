#!/usr/bin/perl

use strict;
use warnings;
use 5.010;
use Cwd;

# given: /path/to/document.pdf
# creates: /path/to/document.index/
# splits each page to: /path/to/document.index/0001.pdf etc...

for my $path_pdf (@ARGV)
{
    next unless -e $path_pdf;
    next unless $path_pdf =~ /\.pdf$/i;
    next if $path_pdf =~ /\.index\//;
    say STDERR $path_pdf;

    my $path_base = $path_pdf;
    $path_base =~ s/\.pdf$//i;

    my $path_index = "$path_base.index";
    next if -d $path_index;
    mkdir $path_index;

    system ('pdfseparate', $path_pdf, $path_index.'/%d.pdf');

    my $old_cwd = cwd();
    chdir $path_index;
    for my $path_file (glob '*.pdf')
    {
        my $i = $path_file;
        $i =~ s/[^0-9]//g;
        next unless $i;
        $i = sprintf ('%04d', $i);
        rename $path_file, "$i.pdf";
        say STDERR "$i.pdf";
    }
    chdir $old_cwd;
}

0;

