#!/usr/bin/perl
use strict;
use warnings;
use 5.010;
use Cwd;

for (<>)
{
    chomp;
    warn $_ unless -e $_;

    my $path_shell = quotemeta $_;
    my $info = `pdfinfo -rawdates $path_shell`;

    $info =~ m/ModDate: *(.*)/;
    my $moddate = $1;
    $info =~ m/CreationDate: *(.*)/;
    $moddate = $1 unless defined $moddate and $moddate =~ m/^D:/;

    # D:20171228141239Z00'00'
    # D:20171101162933
    my ($year, $month, $day) = $moddate =~ m/^D:(....)(..)(..)/;
    my $isodate;
    if (defined $year and defined $month and defined $day)
    {
        $isodate = join '-', $year, $month, $day;
    }
    else
    {
	$isodate = `date -Idate -r $path_shell`;
	chomp $isodate;
	say STDERR "no date found, using file date: $_";
    }

    system('mkdir', '-p', $isodate);
    system('cp', '-b', $_, $isodate);
}

0;
