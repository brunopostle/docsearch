#!/usr/bin/perl
use 5.010;
use strict;
use warnings;
use DBI;

my $path_db = 'words.db';
say "Using database: $path_db";

my $dbh = DBI->connect(          
    "dbi:SQLite:dbname=$path_db", 
    "",                          
    "",                          
    { RaiseError => 1 },         
) or die $DBI::errstr;

my $results;
for my $word (@ARGV)
{
    my $paths = $dbh->selectcol_arrayref ("SELECT Path from Words WHERE Word='$word'");
    for my $path (@$paths)
    {
        $results->{$path}->{$word} = 1;
    }
}

my $drawings = $dbh->selectcol_arrayref ("SELECT Path FROM Papers WHERE (Size='A0' OR Size='A1' OR Size='A2') AND Orientation='Landscape'");

#my @best = sort {scalar keys %{$results->{$b}} <=> scalar keys %{$results->{$a}}} keys %{$results};
my @best = sort keys %{$results};

my $query = join '-', @ARGV;
open HTML, '>', "$query.html";

say HTML "<html><head><title>Drawing Search query '$query'</title></head>";
say HTML "<body><h1>Drawing (A0, A1 &amp; A2 size) Search query '$query'</h1><p>";

for my $path (@best)
{
    my $score = scalar (keys %{$results->{$path}});

    next unless $score == scalar @ARGV;
    next unless grep (/$path/, @{$drawings});

    say STDERR "$score: $path" unless scalar (@ARGV) -1 > $score;
    say HTML "<a href=\"$path\">$path</a>";
    say HTML '<br/>';
}

say HTML '</p></body></html>';
close HTML;

0;

