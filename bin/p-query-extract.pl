#!/usr/bin/perl
use 5.010;
use strict;
use warnings;
use DBI;
use File::Spec;

my $path_db = 'words.db';
say "Using database: $path_db";

my $dbh = DBI->connect(          
    "dbi:SQLite:dbname=$path_db", 
    "",                          
    "",                          
    { RaiseError => 1 },         
) or die $DBI::errstr;

my $results;
for my $word (@ARGV)
{
    my $paths = $dbh->selectcol_arrayref ("SELECT Path from Words WHERE Word='$word'");
    for my $path (@$paths)
    {
        $results->{$path}->{$word} = 1;
    }
}

my $drawings = $dbh->selectcol_arrayref ("SELECT Path FROM Papers WHERE (Size='A0' OR Size='A1' OR Size='A2') AND Orientation='Landscape'");
my %drawings_hash = map {$_ => 1} @{$drawings};

my $letters  = $dbh->selectcol_arrayref ("SELECT Path FROM Papers WHERE Size='A4'");
my %letters_hash = map {$_ => 1} @{$letters};

#my @best = sort {scalar keys %{$results->{$b}} <=> scalar keys %{$results->{$a}}} keys %{$results};
my @best = sort keys %{$results};

my $query = join '-', @ARGV;
open HTML, '>', "$query.html";

say HTML "<html><head><title>Drawing Search query '$query'</title></head><body>";
say HTML "<h1>Drawing (A0, A1 &amp; A2 size) Search query '$query'</h1><p>";

my $count = 0;
for my $path (@best)
{
    my $score = scalar (keys %{$results->{$path}});

    next unless $score == scalar @ARGV;
#    next unless grep (/$path/, @{$drawings});
    next unless exists $drawings_hash{$path};

    my $path_dir = $path;
    $path_dir =~ s/\/[[:alnum:].]+$//;
    $path_dir = File::Spec->canonpath ("/cygdrive/c/Data/$query/drawings/$path_dir");
    system ('mkdir', '-p', $path_dir);
    system ('cp', $path, $path_dir);

    say HTML "<a href=\"$path\">$path</a>";
    say HTML '<br/>';
    $count++;
}

say $count; $count = 0;
say HTML '</p>';
say HTML "<h1>Letters Search query '$query'</h1><p>";

for my $path (@best)
{
    my $score = scalar (keys %{$results->{$path}});

    next unless $score == scalar @ARGV;
    #next if grep (/$path/, @{$drawings});
    #next unless grep (/$path/, @{$letters});
    next unless exists $letters_hash{$path};

    my $path_dir = $path;
    $path_dir =~ s/\/[[:alnum:].]+$//;
    $path_dir = File::Spec->canonpath ("/cygdrive/c/Data/$query/letters/$path_dir");
    system ('mkdir', '-p', $path_dir);
    system ('cp', $path, $path_dir);

    say HTML "<a href=\"$path\">$path</a>";
    say HTML '<br/>';
    $count++;
}

say $count; $count = 0;
say HTML '</p>';
say HTML "<h1>Misc Search query '$query'</h1><p>";

for my $path (@best)
{
    my $score = scalar (keys %{$results->{$path}});

    next unless $score == scalar @ARGV;
    #next if grep (/$path/, @{$drawings});
    #next if grep (/$path/, @{$letters});
    next if exists $drawings_hash{$path};
    next if exists  $letters_hash{$path};

    my $path_dir = $path;
    $path_dir =~ s/\/[[:alnum:].]+$//;
    $path_dir = File::Spec->canonpath ("/cygdrive/c/Data/$query/misc/$path_dir");
    system ('mkdir', '-p', $path_dir);
    system ('cp', $path, $path_dir);

    say HTML "<a href=\"$path\">$path</a>";
    say HTML '<br/>';
    $count++;
}

say $count; $count = 0;
say HTML '</p></body></html>';

0;

