#!/usr/bin/perl
use 5.010;
use strict;
use warnings;
use DBI;
use Text::Aspell;

my $speller = new Text::Aspell;
$speller->set_option('lang','en');

my $path_db = 'words.db';
say "Using database: $path_db";

my $dbh = DBI->connect(          
    "dbi:SQLite:dbname=$path_db", 
    "",                          
    "",                          
    { RaiseError => 1, AutoCommit => 0 },         
) or die $DBI::errstr;

#$dbh->do("DROP TABLE IF EXISTS Words");
$dbh->do("CREATE TABLE Words(Id INTEGER PRIMARY KEY, Word TEXT, Path TEXT)") unless (grep /Words/, $dbh->tables);

my $word_count = 0;
say "Documents: ". scalar @ARGV;

for my $path (@ARGV)
{
    next unless $path =~ /\.txt$/;
    warn "File not found:\n$path" unless -e $path;
    next unless -e $path;
    my $path_base = $path;
    $path_base =~ s/\.txt$//;

    my $path_orig;
    for my $extension (qw/pdf xls xlsx doc docx msg/)
    {
        $path_orig = $dbh->quote ("$path_base.$extension") if -e "$path_base.$extension";
    }

    my @rv = $dbh->selectrow_array("SELECT Path FROM Words WHERE Path = $path_orig");
    next if scalar @rv;

    my @words = get_words ($path);
    for my $word (@words)
    {
        $word =~ s/[^[:alpha:]]+$//;
        $word =~ s/^[^[:alpha:]]+//;
        next unless $word =~ /^[[:alpha:]]{3,}$/;
        next unless ($speller->check ($word) or $word =~ /^[A-Z]/);
        $word = lc $word;
        $dbh->do("INSERT INTO Words(Word, Path) VALUES('$word', $path_orig)");
        $word_count++;
    }
}

$dbh->commit();
$dbh->disconnect();
say "Words: $word_count";

sub get_words
{
    my $path = shift;
    open TEXT, '<', $path;
    my $text = do { local $/; <TEXT>};
    close TEXT;
    my @words = $text =~ /([^[:space:]]+)/g;
    my $hash = {};
    for my $word (@words) {$hash->{$word} = 1};
    #return @words;
    return keys %{$hash};
}

0;

