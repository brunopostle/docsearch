#!perl
use 5.010;
use strict;
use warnings FATAL => 'all';
use Test::More 'no_plan';
use lib 'lib';

ok (!system ('which', 'antiword'), 'antiword');
ok (!system ('which', 'pdfinfo'), 'pdfinfo');
ok (!system ('which', 'mkdir'), 'mkdir');
ok (!system ('which', 'pdfseparate'), 'pdfseparate');
ok (!system ('which', 'unzip'), 'unzip');
ok (!system ('which', '7z'), '7z');
ok (!system ('which', 'pdftotext'), 'pdftotext');
ok (!system ('which', 'pdftoppm'), 'pdftoppm');
ok (!system ('which', 'convert'), 'convert');
ok (!system ('which', 'tesseract'), 'tesseract');
ok (!system ('which', 'msgconvert'), 'msgconvert');
ok (!system ('which', 'xls2csv'), 'xls2csv');
ok (!system ('which', 'unix2dos'), 'unix2dos');
ok (!system ('which', 'cp'), 'cp');

